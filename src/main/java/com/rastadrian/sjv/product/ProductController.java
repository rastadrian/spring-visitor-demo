package com.rastadrian.sjv.product;

import com.rastadrian.sjv.product.discount.Discount;
import com.rastadrian.sjv.product.discount.ProductDiscountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
public class ProductController {

    private final ProductDiscountService productDiscountService;

    @PostMapping(value = "/discount",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Discount> discount(@RequestBody Product product) {
        return ResponseEntity.of(product.accept(productDiscountService));
    }
}
