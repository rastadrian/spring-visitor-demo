package com.rastadrian.sjv.product.discount;

import com.rastadrian.sjv.product.type.Book;
import com.rastadrian.sjv.product.type.ManufacturedProduct;
import com.rastadrian.sjv.product.type.ProductVisitor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductDiscountService implements ProductVisitor<Optional<Discount>> {

    @Override
    public Optional<Discount> visit(Book book) {
        return Optional.empty();
    }

    @Override
    public Optional<Discount> visit(ManufacturedProduct manufacturedProduct) {
        return Optional.empty();
    }
}
