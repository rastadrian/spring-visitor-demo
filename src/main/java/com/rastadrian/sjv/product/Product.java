package com.rastadrian.sjv.product;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.rastadrian.sjv.product.type.Book;
import com.rastadrian.sjv.product.type.ManufacturedProduct;
import com.rastadrian.sjv.product.type.ProductVisitor;
import lombok.Getter;
import lombok.Setter;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;

@Getter
@Setter
@JsonTypeInfo(use = NAME, property = "type")
@JsonSubTypes({
        @Type(name = "book", value = Book.class),
        @Type(name = "manufactured_product", value = ManufacturedProduct.class)
})
public abstract class Product {

    private String type;

    public abstract <T> T accept(ProductVisitor<T> visitor);
}
