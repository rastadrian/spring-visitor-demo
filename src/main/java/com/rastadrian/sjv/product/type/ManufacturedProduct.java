package com.rastadrian.sjv.product.type;

import com.rastadrian.sjv.product.Product;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ManufacturedProduct extends Product {

    private String upc;
    private String name;
    private String manufacturer;

    @Override
    public <T> T accept(ProductVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
