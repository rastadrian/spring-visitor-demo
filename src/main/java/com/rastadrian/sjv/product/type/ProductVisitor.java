package com.rastadrian.sjv.product.type;

public interface ProductVisitor<T> {

    T visit(Book book);

    T visit(ManufacturedProduct manufacturedProduct);
}
