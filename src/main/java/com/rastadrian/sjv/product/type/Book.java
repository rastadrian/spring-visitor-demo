package com.rastadrian.sjv.product.type;

import com.rastadrian.sjv.product.Product;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Book extends Product {

    private String isbn;
    private String title;
    private Integer numberOfPages;

    @Override
    public <T> T accept(ProductVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
