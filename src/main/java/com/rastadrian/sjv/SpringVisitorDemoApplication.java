package com.rastadrian.sjv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringVisitorDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringVisitorDemoApplication.class, args);
    }

}
