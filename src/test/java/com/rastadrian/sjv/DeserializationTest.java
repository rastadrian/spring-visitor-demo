package com.rastadrian.sjv;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rastadrian.sjv.product.Product;
import com.rastadrian.sjv.product.type.Book;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DeserializationTest {
    @Test
    void jacksonShouldDeserializeAsSubTypes() throws JsonProcessingException {
        // given
        ObjectMapper objectMapper = new ObjectMapper();
        String book = "{\"type\": \"book\", \"isbn\": \"5012349535\"}";

        // when
        Product product = objectMapper.readValue(book, Product.class);

        // then
        assertThat(product).isInstanceOf(Book.class);
        assertThat(((Book) product).getIsbn()).isEqualTo("5012349535");
    }
}
